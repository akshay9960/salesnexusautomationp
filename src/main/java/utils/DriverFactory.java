package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class DriverFactory {
    private static ThreadLocal<WebDriver> tl_driver = new ThreadLocal<WebDriver>();

    public synchronized static WebDriver getDriver() {
        if (tl_driver.get() == null) {
            tl_driver.set(initializeDriver());
        }
        return tl_driver.get();
    }

    /**
     * Method used to launch browser specified in Configuration.properties file
     * and to launch Url which is also specified in Configuration.properties file
     * @return instance of browser
     */
    private synchronized static WebDriver initializeDriver() {
        WebDriver driver = null;
        String browserName = ConfigurationReader.getProperty("browserName");

        switch (browserName) {
            case "chrome": {
                // System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"src/main/java/driver/drivers/chromedriver.exe");
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
                options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
                driver = new ChromeDriver(options);
                driver.manage().window().maximize();
                break;
            }
            case "firefox": {
                // System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"src/main/java/driver/drivers/chromedriver.exe");
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions options = new FirefoxOptions();
                options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
                driver = new FirefoxDriver(options);
                driver.manage().window().maximize();
                break;
            }
            case "edge": {
                WebDriverManager.edgedriver().setup();
                EdgeOptions options = new EdgeOptions();
                options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
                driver = new EdgeDriver(options);
                driver.manage().window().maximize();
                break;
            }

        }
        return driver;
    }

    /**
     * This method is used close
     * browser window
     */
    public static void cleanup() {
        tl_driver.get().quit();
        tl_driver.remove();
    }
}