package utils;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExcelReader {
    public static String getCellValue(int row, int column, String sheetName) throws IOException {

        FileInputStream in = new FileInputStream("./src/main/resources/SalesNexusData.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(in);
        XSSFSheet sheet = workbook.getSheet(sheetName);
        XSSFCell cell = sheet.getRow(row).getCell(column);

        switch (cell.getCellType()) {
            case FORMULA:
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                double value = cell.getNumericCellValue();
                int intValue = (int) value;
                String answer = Integer.toString(intValue);
                return answer;
            default:
                return "No";
        }
    }

    public static synchronized void setCellValue(String data,int column,int row){
        String sheetName="Accounts";
        try {
            FileInputStream fis = new FileInputStream("./src/main/resources/UserAccounts.xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheet(sheetName);
            XSSFCell cell = sheet.getRow(row).createCell(column);
            cell.setCellValue(data);
            FileOutputStream fos=null;
            try{
                fos = new FileOutputStream(new File("./src/main/resources/UserAccounts.xlsx"));
                workbook.write(fos);
            }catch (IOException e){
                System.out.println(e.getStackTrace());
            }
            fis.close();
            fos.close();
        }catch (IOException e){
            System.out.println(e.getStackTrace());
        }
    }
}
