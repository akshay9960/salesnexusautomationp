package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigurationReader {
    static FileInputStream fis;
    static Properties prop;

    /**@author Akshay
     * This method
     * @return Properties class object initialize with configuration.properties file
     */
    public static Properties initializePropertyObject() {
        try {
            fis = new FileInputStream("./src/main/resources/Configuration.properties");
            prop = new Properties();
            prop.load(fis);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return prop;
    }

    /**
     * This method is used to fetch
     * data from configuration.properties file
     * @return value of properties stored in configuration file
     */
    public static String getProperty(String property) {
        Properties prop = initializePropertyObject();
        return prop.getProperty(property);
    }

}
