package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import utils.ConfigurationReader;

public class LoginPage extends BasePage {
    public LoginPage() {
        super();
    }

    WebDriver driver = getDriver();

    //WebElements
    @FindBy(xpath = "//input[@id='user']")
    private WebElement emailInputField;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordInputField;

    @FindBy(xpath = "//input[@id='btnLogin']")
    private WebElement loginBtn;

    @FindBy(xpath = "//a[text()='Skip Intro']")
    private WebElement skipIntroBTn;

    @FindBy(xpath = "//h4[text()='Contacts']")
    private WebElement contactHeading;

    @FindBy(xpath="//h2[text()='Alert: Other devices are using your account']")
    private WebElement otherDeviceUsingAccountHeading;

    @FindBy(xpath="//input[@value='Okay']")
    private WebElement okBtn;

    @FindBy(xpath="//li[@id='hdrXMenu']")
    private WebElement menuBtn;

    @FindBy(xpath="//a[text()='Logout']")
    private WebElement logOut;


    //action methods
    public void navigateToLoginPage() {
        String loginUrl = ConfigurationReader.getProperty("loginUrl");
        navigateToUrl(loginUrl);
    }

    public void login() {
        String loginEmail = ConfigurationReader.getProperty("username");
        String password = ConfigurationReader.getProperty("password");
        sendKeys(emailInputField, loginEmail);
        sendKeys(passwordInputField, password);
    }

    public void clickOnLoginButton() {
        waitAndClickByJs(loginBtn);
        handleOtherDeviceUsingWindowPopup();
    }

    public void verifyUserIsOnHomePage() {
        waitForElementToBeVisible(contactHeading);
        Assert.assertTrue(contactHeading.isDisplayed());
        System.out.println("user is on homepage");
    }

    /**
     * This method handles
     * other device logged in pop up
     */
    public void handleOtherDeviceUsingWindowPopup(){
        try{
            waitForElementToBeVisible(otherDeviceUsingAccountHeading);
            if(otherDeviceUsingAccountHeading.isDisplayed()){
                okBtn.click();
            }
        }
        catch(Exception ne){
            System.out.println(ne.getStackTrace());
            System.out.println("No other device is logged in");

        }
        waitAndClickByJs(skipIntroBTn);
    }

    public void logOut(){
        waitForElementToBeVisible(menuBtn);
        menuBtn.click();
        waitAndClickByJs(logOut);
    }
}
