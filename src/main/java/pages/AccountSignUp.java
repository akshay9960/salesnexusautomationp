package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.ConfigurationReader;
import utils.ExcelReader;

import java.util.HashMap;
import java.util.Map;

public class AccountSignUp extends BasePage {

    public AccountSignUp() {
        super();
    }

    Map<String, String> accountDetails = new HashMap<>();
    int rowNumber;

    //WebElements
    @FindBy(xpath = "//input[@id='fullName']")
    private WebElement fullNameField;

    @FindBy(xpath = "//input[@id='phone']")
    private WebElement phoneNoField;

    @FindBy(xpath = "//input[@id='em']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement submitBtn;

    @FindBy(xpath = "//input[@id='username']")
    private WebElement userNameField;

    @FindBy(xpath = "//input[@id='email']")
    private WebElement emailTextBox;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//input[@id='confirm-password']")
    private WebElement confirmPasswordField;

    @FindBy(xpath = "//div[text()='Wanna change your password?']")
    private WebElement changeYourPaswordHeading;

    @FindBy(xpath = "//a[@id='gsw-credentials-btn']")
    private WebElement continueBtn;

    @FindBy(xpath = "//div[@class='intro-title']")
    private WebElement customizeExperienceHeading;

    @FindBy(xpath = "//a[@id='gsw-intro-btn']")
    private WebElement organizeCustBtn;

    @FindBy(xpath = "//span[text()='Add a field...']")
    private WebElement addAFieldDropDown;

    @FindBy(xpath = "//a[@id='gsw-next-step']")
    private WebElement nextStepBtn;

    @FindBy(xpath = "//div[text()='Set Up Your Company Branding']")
    private WebElement setUpCompanyBrandHeading;

    @FindBy(xpath = "//input[@id='txt-company-name']")
    private WebElement companyNameField;

    @FindBy(xpath = "//input[@id='txt-address1']")
    private WebElement addressLine1;

    @FindBy(xpath = "//input[@id='txt-address2']")
    private WebElement addressLine2;

    @FindBy(xpath = "//input[@id='txt-city']")
    private WebElement cityField;

    @FindBy(xpath = "//input[@id='txt-zip']")
    private WebElement zipCodeField;

    @FindBy(xpath = "//input[@id='txt-state']")
    private WebElement stateField;

    @FindBy(xpath = "//input[@id='txt-phone']")
    private WebElement phoneNumField;

    @FindBy(xpath = "//input[@id='txt-website']")
    private WebElement websiteField;

    @FindBy(xpath = "//input[@id='team-size']")
    private WebElement teamSizeField;

    @FindBy(xpath = "//a[@id='btnSaveCompany']")
    private WebElement importContactsBtn;

    @FindBy(xpath = "//div[text()='Import Contacts']")
    private WebElement importContactsHeading;

    @FindBy(xpath = "//a[@id='btnContinue']")
    private WebElement freeTrialBtn;

    @FindBy(xpath = "//a[text()='Skip Intro']")
    private WebElement skipIntroBTn;


    //Action methods

    /**
     * This method is used to navigate
     * to Account signup page
     */
    public void navigateToAccountSignUpPage() {
        String url = ConfigurationReader.getProperty("url");
        navigateToUrl(url);
    }

    /**
     * This method is used for
     * providing signup details
     */
    public void signupDetails(String row) {
        rowNumber = Integer.parseInt(row);
        String firstName = generateRandomString(4);
        accountDetails.put("FirstName", firstName);
        String lastName = generateRandomString(5);
        String fullName = firstName + " " + lastName;
        String phoneNumber = generateRandomNumbers(10);
        String email = (firstName + lastName + generateRandomNumbers(3)).toLowerCase() + "@gmail.com";
        //writing name in excel file
        setFullName(fullName, rowNumber);
        sendKeys(fullNameField, fullName);
        //writing phone no in excel file
        setPhoneNumber(phoneNumber, rowNumber);
        sendKeys(phoneNoField, "(973)248-3914");
        setEmail(email, rowNumber);
        sendKeys(emailField, email);
        waitAndClickByJs(submitBtn);
    }

    public void changeDefaultPassword() {
        String password = generateRandomNumbers(3) + generateRandomString(3);
        waitForElementToBeVisible(changeYourPaswordHeading);
        setPassword(password, rowNumber);
        sendKeys(passwordField, password);
        sendKeys(confirmPasswordField, password);
        waitAndClickByJs(continueBtn);
    }

    public void clickOnOrganizeCustomerBtn() {
        waitForElementToBeVisible(customizeExperienceHeading);
        waitAndClickByJs(organizeCustBtn);
    }

    public void nextStepBtn() {
        waitForElementToBeVisible(addAFieldDropDown);
        waitAndClickByJs(nextStepBtn);
    }

    public void companyBrandingDetails(String cityName, String stateName, String zipCode) {
        waitForElementToBeVisible(setUpCompanyBrandHeading);
        String companyName = generateRandomString(6) + "pvt.ltd";
        String address1 = "92 Plaza Lane, 2/3 Building A";
        String phoneNumber = generateRandomNumbers(7);
        String website = companyName + ".com";
        String teamSize = "10";
        sendKeys(companyNameField, companyName);
        sendKeys(addressLine1, address1);
        sendKeys(cityField, cityName);
        sendKeys(stateField, stateName);
        sendKeys(zipCodeField, zipCode);
        sendKeys(phoneNumField, phoneNumber);
        sendKeys(websiteField, website);
        sendKeys(teamSizeField, teamSize);
        waitAndClickByJs(importContactsBtn);
    }

    public void clickOnTakeMeToFreeTrialBtn() {
        waitForElementToBeVisible(importContactsHeading);
        waitAndClickByJs(freeTrialBtn);
    }

    public void verifyUserFirstNameInContactsTable() {
        String userName = accountDetails.get("FirstName");
        waitForElementToBeVisibleBy(By.xpath("//td[text()='" + userName + "']"));
        System.out.println("Name of the user found in the table");
    }

    public void setFullName(String data, int row) {
        ExcelReader.setCellValue(data, 1, row);
    }

    public void setPhoneNumber(String data, int row) {
        ExcelReader.setCellValue(data, 2, row);
    }

    public void setEmail(String data, int row) {
        ExcelReader.setCellValue(data, 3, row);
    }

    public void setPassword(String data, int row) {
        ExcelReader.setCellValue(data, 4, row);
    }

}
