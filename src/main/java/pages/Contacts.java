package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import utils.ExcelReader;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Contacts extends BasePage{
    public Contacts(){
        super();
    }
    WebDriver driver=getDriver();
    Map<String,String> userDetails=new HashMap<>();

    //WebElements
    @FindBy(xpath="//a[text()='Contacts']")
    private WebElement contactsTab;

    @FindBy(xpath="//a[text()='Create Contact']")
    private WebElement createContactsOption;

    @FindBy(xpath="//h4[text()='Create Contact']")
    private WebElement createContactHeading;

    @FindBy(xpath="//input[@id='f_25']")
    private WebElement companyNameField;

    @FindBy(xpath="//input[@id='f_26']")
    private WebElement contactName;

    @FindBy(xpath="//input[@id='f_35']")
    private WebElement phoneNoInputField;

    @FindBy(xpath="//input[@id='f_200']")
    private WebElement emailAddressField;

    @FindBy(xpath="//input[@id='f_94']")
    private WebElement websiteInputField;

    @FindBy(xpath="//input[@id='f_27']")
    private WebElement address1Field;

    @FindBy(xpath="//input[@id='f_80']")
    private WebElement phoneExtField;

    @FindBy(xpath="//input[@id='f_38']")
    private WebElement mobileNoField;

    @FindBy(xpath="//div[@id='f_46-button']//option/following-sibling::option")
    private List<WebElement> titles;

    @FindBy(xpath="//div[@id='f_34-button']//option/following-sibling::option")
    private List<WebElement> statuses;

    @FindBy(xpath="//input[@id='f_109']")
    private WebElement referedByField;

    @FindBy(xpath="//div[@id='f_49-button']//option/following-sibling::option")
    private List<WebElement> leadSources;

    @FindBy(xpath="//div[@id='f_30-button']//option/following-sibling::option")
    private List<WebElement> cities;

    @FindBy(xpath="//div[@id='f_31-button']//option/following-sibling::option")
    private List<WebElement> states;

    @FindBy(xpath="//input[@id='f_32']")
    private WebElement zipCode;

    @FindBy(xpath="//a[text()='Save']")
    private WebElement saveBtn;

    @FindBy(xpath="//a[text()='View All Contacts']")
    private WebElement viewAllContacts;

    @FindBy(xpath="//input[@id='searchForContact']")
    private WebElement contactSearchBox;

    @FindBy(xpath="//i[@class='icon-large icon-remove']")
    private WebElement deleteContactIcon;

    @FindBy(xpath="//label[text()='No contacts found for your search.']")
    private WebElement noContactErrorMsg;



    //Create lookk up webelements
    @FindBy(xpath="//h4[text()='Create Lookup']")
    private WebElement createLookUpHeading;

    @FindBy(xpath="//a[text()='Create Lookup']")
    private WebElement createLookUpOption;

    @FindBy(xpath="//div[@id='select-101-button']")
    private WebElement twoRow1stcoldropdown;

    @FindBy(xpath="//div[@id='select-102-button']/span")
    private WebElement twoRow2ndCol;

    @FindBy(xpath="//input[@name='sCONVALUE1']")
    private WebElement twoRow3rdColinputField;

    @FindBy(xpath="//div[@id='select-103-button']")
    private WebElement thirdRow1stcoldropdown;

    @FindBy(xpath="//select[@name='sCONOPERATION3']")
    private WebElement thirdRow2ndCol;

    @FindBy(xpath="//select[@name='sCONVALUE2']")
    private WebElement  thirdRow3rdColinputField ;

   @FindBy(xpath="//div[@id='select-101-button']/select/option")
   private List<WebElement> twoRow1stcoldropdownOptions;

   @FindBy(xpath="//select[@name='sCONOPERATION1']/option")
   private List<WebElement> twoRow2ndColdropdownOptions;

   @FindBy(xpath="//div[@id='select-103-button']/select/option")
   private List<WebElement> thirdRow1stcoldropdownOptions;

   @FindBy(xpath="//select[@name='sCONOPERATION2']/option")
   private List<WebElement> thirdRow2ndColOptions;

   @FindBy(xpath="//a[text()='Search ']")
   private WebElement searchBtn;

   @FindBy(xpath="//a[text()='Make This My Lookup']")
   private WebElement makeThisMyLookUpBtn;

   @FindBy(xpath="//div[@id='select-98-button']/span")
   private WebElement lookupUserPost;

   @FindBy(xpath="//div[@id='select-100-button']/span")
   private WebElement lookupUserName;





    //Action methods
    public void clickOnCreateContactOption(){
        contactsTab.click();
        waitAndClickByJs(createContactsOption);
    }
    public void verifyUserIsOnCreateContactWindow(){
        waitForElementToBeVisible(createContactHeading);
        Assert.assertTrue(createContactHeading.isDisplayed());
    }
    public void enterCreateContactDetails(String name,String title, String status, String leadSource, String city, String state){
        String contactN=name+generateRandomString(4).toLowerCase();
        //entering contacts name
        userDetails.put("contactName",contactN);
        sendKeys(contactName,contactN);
        //using stream api to select contact's title from title dropdown
        titles.stream().filter(s->s.getText().equalsIgnoreCase(title)).findFirst().orElseThrow(null).click();
        //entering company name
        String companyName=generateRandomString(4)+" Pvt Ltd";
        sendKeys(companyNameField,companyName);
        //entering phone number
        sendKeys(phoneNoInputField,"002"+generateRandomNumbers(4));
        //entering email address
        sendKeys(emailAddressField,contactN+generateRandomNumbers(3)+"@gmail.com");
        //entering website
        sendKeys(websiteInputField,"www."+companyName+".com");
        //entering phone ext.
        sendKeys(phoneExtField,"(0256)7899919");
        //entering mobile number
        sendKeys(mobileNoField,"0112256613");
        //using stream api to select contact's status from status dropdown
        statuses.stream().filter(e->e.getText().equalsIgnoreCase(status)).findFirst().orElseThrow(null).click();
        //entering refered by
        sendKeys(referedByField,generateRandomString(5));
        //using stream api to select contact's lead source from lead dropdown
        leadSources.stream().filter(e->e.getText().equalsIgnoreCase(leadSource)).findFirst().orElseThrow(null).click();
        //entering contact's address
        sendKeys(address1Field,"92 Plaza Lane");
        //using stream api to select contact's city from city dropdown
        cities.stream().filter(e->e.getText().equalsIgnoreCase(city)).findFirst().orElseThrow(null).click();
        //using stream api to select contact's state from state dropdown
        states.stream().filter(e->e.getText().equalsIgnoreCase(state)).findFirst().orElseThrow(null).click();
        //entering zip code
        sendKeys(zipCode,"32109");
        waitAndClickByJs(saveBtn);
    }

    public synchronized void searchContactInContactSearch() throws InterruptedException {
        String savedContactName = userDetails.get("contactName");
        Thread.sleep(5000);
        contactsTab.click();
        waitForElementToBeVisible(viewAllContacts);
        viewAllContacts.click();
        sendKeys(contactSearchBox, savedContactName);
        try {
            waitForElementToBeVisibleBy(By.xpath("//td[text()='" + savedContactName + "']"));
            String actualContactName = driver.findElement(By.xpath("//td[text()='" + savedContactName + "']")).getText();
            Assert.assertEquals(savedContactName, actualContactName);
            System.out.println("Contact name found in the contacts table");
        }
        catch(Exception e){
            System.out.println(e.getStackTrace());
            System.out.println("contact is not available or has been deleted");
        }
    }
    public void deleteContact() throws InterruptedException {
        Thread.sleep(3000);
        waitForElementToBeVisible(deleteContactIcon);
        deleteContactIcon.click();
        waitForTheAlertAndPerformAction("accept");
    }
    public void verifyContactIsDeleted(String deleteMsg){
        waitForElementToBeVisible(noContactErrorMsg);
        String actualErrorMsg=noContactErrorMsg.getText();
        Assert.assertEquals(deleteMsg,actualErrorMsg);
        System.out.println("Error msg displayed to user");
    }
    public void clickOnCreateLookUp(){
        contactsTab.click();
        waitAndClickByJs(createLookUpOption);
    }
    public void verifyUserIsOnCreateLookUpWindow(){
        waitForElementToBeVisible(createLookUpHeading);
        Assert.assertTrue(createLookUpHeading.isDisplayed());
    }
    public void verifyPostAndNameOfUserOnLookupWindow(){
        String userName= userDetails.get("contactName");
        System.out.println("username "+userName);
        String post="Record Manager";
        String actualPost=lookupUserPost.getText();
        String actualUserName=lookupUserName.getText();
        Assert.assertEquals(post,actualPost);
        Assert.assertEquals("Sadio Mane",actualUserName);
        System.out.println("Post and user name verification successfully");
    }

    public void clickOnSearchAndLookUpBtn(){
        waitAndClickByJs(searchBtn);
        waitAndClickByJs(makeThisMyLookUpBtn);
    }

    public void setValuesInFirsRowDropDowns(String data,String condition,String inputText) {
        waitForElementToBeVisible(twoRow1stcoldropdown);
        twoRow1stcoldropdown.click();
        twoRow1stcoldropdownOptions.stream().filter(e -> e.getText().equalsIgnoreCase(data)).findFirst().orElseThrow(null).click();

        try {
            //waitForElementToBeVisible(twoRow2ndCol);
            twoRow2ndCol.click();

        }
        catch (Exception ae){
            ae.printStackTrace();
            driver.findElement(By.xpath("//select[@name='sCONOPERATION1']")).click();
            System.out.println("Hi");
        }
        twoRow2ndColdropdownOptions.stream().filter(e -> e.getText().equalsIgnoreCase(condition)).findFirst().orElseThrow(null).click();
        sendKeys(twoRow3rdColinputField, inputText);
    }
    public void setValuesInSecondRowDropDowns(String data,String condition,String inputText){
        waitForElementToBeVisible(thirdRow1stcoldropdown);
        thirdRow1stcoldropdown.click();
        thirdRow1stcoldropdownOptions.stream().filter(e->e.getText().equalsIgnoreCase(data)).findFirst().orElseThrow(null).click();
       // waitForElementToBeVisible(thirdRow2ndCol);
        try{
            thirdRow2ndCol.click();
        }
        catch(Exception e){
            e.printStackTrace();
            driver.findElement(By.xpath("//select[@name='sCONOPERATION3']")).click();
        }

        thirdRow2ndColOptions.stream().filter(e->e.getText().equalsIgnoreCase(condition)).findFirst().orElseThrow(null).click();
        thirdRow3rdColinputField.click();
        List<WebElement>cities=driver.findElements(By.xpath("//select[@name='sCONVALUE2']/option"));
        cities.stream().filter(e->e.getText().equalsIgnoreCase(inputText)).findFirst().orElseThrow(null).click();

    }
    public void fillConatctDetailsFromExcelFile(String rowNumber) throws IOException {
        int row =Integer.parseInt(rowNumber);
        String name= ExcelReader.getCellValue(row,1,"Contacts");
        String lastName=ExcelReader.getCellValue(row,2,"Contacts");
        String fullName=name+" "+lastName+generateRandomString(3).toLowerCase();
        userDetails.put("contactName",fullName);
        String title=ExcelReader.getCellValue(row,3,"Contacts");
        String status=ExcelReader.getCellValue(row,10,"Contacts");
        String leadSource=ExcelReader.getCellValue(row,11,"Contacts");
        String city=ExcelReader.getCellValue(row,13,"Contacts");
        String state=ExcelReader.getCellValue(row,14,"Contacts");
        enterCreateContactDetailsFromFile(row,fullName,title,status,leadSource,city,state);
    }
    public void enterCreateContactDetailsFromFile(int row,String name,String title, String status, String leadSource, String city, String state) throws IOException {
        //entering contacts name
        sendKeys(contactName,name);
        //using stream api to select contact's title from title dropdown
        titles.stream().filter(s->s.getText().equalsIgnoreCase(title)).findFirst().orElseThrow(null).click();
        //entering company name
        String companyName=ExcelReader.getCellValue(row,4,"Contacts");
        sendKeys(companyNameField,companyName);
        //entering phone number
        String phone=ExcelReader.getCellValue(row,5,"Contacts");
        sendKeys(phoneNoInputField,phone);
        //entering email address
        String emailAddress=ExcelReader.getCellValue(row,6,"Contacts");
        sendKeys(emailAddressField,emailAddress);
        //entering website
        String website=ExcelReader.getCellValue(row,7,"Contacts");
        sendKeys(websiteInputField,website);
        //entering phone ext.
        String phoneExt=ExcelReader.getCellValue(row,8,"Contacts");
        sendKeys(phoneExtField,phoneExt);
        //entering mobile number
        String mobilePhone=ExcelReader.getCellValue(row,9,"Contacts");
        sendKeys(mobileNoField,"0112256613");
        //using stream api to select contact's status from status dropdown
        statuses.stream().filter(e->e.getText().equalsIgnoreCase(status)).findFirst().orElseThrow(null).click();
        //entering refered by
        sendKeys(referedByField,generateRandomString(5));
        //using stream api to select contact's lead source from lead dropdown
        leadSources.stream().filter(e->e.getText().equalsIgnoreCase(leadSource)).findFirst().orElseThrow(null).click();
        //entering contact's address
        String address=ExcelReader.getCellValue(row,12,"Contacts");
        sendKeys(address1Field,address);
        //using stream api to select contact's city from city dropdown
        cities.stream().filter(e->e.getText().equalsIgnoreCase(city)).findFirst().orElseThrow(null).click();
        //using stream api to select contact's state from state dropdown
        states.stream().filter(e->e.getText().equalsIgnoreCase(state)).findFirst().orElseThrow(null).click();
        //entering zip code
        String zip=ExcelReader.getCellValue(row,15,"Contacts");
        sendKeys(zipCode,zip);
        waitAndClickByJs(saveBtn);
    }



}
