package pages;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.DriverFactory;
import utils.Global_Const;

import java.time.Duration;

import static utils.DriverFactory.getDriver;

public class BasePage {

    /**
     * This constructor is called by subpages
     * to initialize page WebElements with WebDriver instance
     */
    public BasePage() {
        PageFactory.initElements(getDriver(), this);
    }

    /**
     * This method
     * @return current WebDriver instance
     */
    public static WebDriver getDriver(){
        return DriverFactory.getDriver();
    }

    /**
     * This method is
     * used to navigate to given url
     * @param url
     */
    public void navigateToUrl(String url){
        getDriver().get(url);
    }

    /**
     * This method clicks on
     * element using javascriptexecutor interface
     * @param element
     */
    public void waitAndClickByJs(WebElement element){
      try{
          WebDriverWait wt=new WebDriverWait(getDriver(), Duration.ofSeconds(Global_Const.Explicit_WAIT));
          wt.until(ExpectedConditions.elementToBeClickable(element));
          JavascriptExecutor jsExecutor=(JavascriptExecutor)getDriver();
          jsExecutor.executeScript("arguments[0].click();", element);
          System.out.println("Successfully clicked on element");
      }
      catch(Exception e){
          System.out.println("Element" +element.toString()+ "  not clickable");
      }
    }

    /**
     * This method is used
     * to send text to input field
     * @param element
     * @param textInput
     */
    public void sendKeys(WebElement element, String textInput) {
        WebDriverWait wt = new WebDriverWait(getDriver(), Duration.ofSeconds(Global_Const.Explicit_WAIT));
        wt.until(ExpectedConditions.elementToBeClickable(element)).sendKeys(textInput);
    }

    /**
     * This method is used to
     * send text to input using locator
     * @param by
     * @param textInput
     */
    public void sendKeys(By by, String textInput) {
        WebDriverWait wt = new WebDriverWait(getDriver(), Duration.ofSeconds(Global_Const.Explicit_WAIT));
        wt.until(ExpectedConditions.elementToBeClickable(by)).sendKeys(textInput);
    }

    public String generateRandomNumbers(int length) {
        return RandomStringUtils.randomNumeric(length);
    }

    public String generateRandomString(int length) {
        return RandomStringUtils.randomAlphabetic(length);
    }

    public void waitForElementToBeVisible(WebElement element){
        WebDriverWait wt = new WebDriverWait(getDriver(), Duration.ofSeconds(Global_Const.Explicit_WAIT));
        wt.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForElementToBeVisibleBy(By by){
        WebDriverWait wt = new WebDriverWait(getDriver(), Duration.ofSeconds(Global_Const.Explicit_WAIT));
        wt.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    /**
     * This method is used to perform
     * action on alert pop up
     * @param actionType
     */
    public void waitForTheAlertAndPerformAction(String actionType) {
        WebDriverWait wt = new WebDriverWait(getDriver(), Duration.ofSeconds(Global_Const.Explicit_WAIT));
        wt.until(ExpectedConditions.alertIsPresent());
        switch (actionType) {
            case "accept": {
                getDriver().switchTo().alert().accept();
                break;
            }
            case "dismiss": {
                getDriver().switchTo().alert().dismiss();
                break;
            }
            default:{
                System.out.println("Enter valid action");
            }

        }
    }
}




