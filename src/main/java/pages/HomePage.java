package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage extends BasePage {
    public HomePage() {
        super();
    }

    WebDriver driver = getDriver();

    //WebElements
    @FindBy(xpath = "//a[text()='Skip Intro']")
    private WebElement skipIntroBtn;

    @FindBy(xpath = "//input[@id='searchForContact']")
    private WebElement contactSearchBox;

    //Action methods
    public void verifyUserIsOnHomePage() {
        waitAndClickByJs(skipIntroBtn);
        waitForElementToBeVisible(contactSearchBox);
        Assert.assertTrue(contactSearchBox.isDisplayed());
        System.out.println("user is on salesNexus Home Page");
    }

    public void clickOnSkipIntroBtn() {
        try {
            waitForElementToBeVisible(skipIntroBtn);
            if(skipIntroBtn.isDisplayed())  skipIntroBtn.click();

        } catch (NoSuchElementException ne) {
            System.out.println(ne.getStackTrace());
            System.out.println(skipIntroBtn.toString()+"element did not appear");
        }

    }
}