package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Locale;

public class Tasks extends BasePage {
    public Tasks(){
        super();
    }

    //WebElements
    @FindBy(xpath="//a[text()='Tasks']")
    private WebElement taskTab;

    @FindBy(xpath="(//ul[@id='hdrTasksUL']/li)[3]")
    private WebElement toDo;

    @FindBy(xpath="//input[@id='contactSearch']")
    private WebElement searchBy;

    @FindBy(xpath="//a[@id='activityType-button']")
    private WebElement activityBTn;

    @FindBy(xpath="//ul[@id='activityType-menu']/li/a")
    private List<WebElement> activityTypeMenu;

    @FindBy(xpath="//input[@id='scheduleDate_instance']")
    private WebElement dateBtn;

    @FindBy(xpath="//input[@id='scheduleTime_instance']")
    private WebElement timePickerFromBtn;

    @FindBy(xpath="//ul[@class='ui-timepicker-list']/li")
    private List<WebElement> timeOptions;

    @FindBy(xpath="//input[@id='scheduleTimeEndTime_instance']")
    private WebElement getTimePickerToBtn;

    @FindBy(xpath="//select[@id='regarding']")
    private WebElement regardingDropDown;

    @FindBy(xpath="//select[@id='regarding']/option")
    private List<WebElement> regardingOptions;

    @FindBy(xpath="//*[@id='details']")
    private WebElement detailsTextArea;

    @FindBy(xpath="//div[@id='delegateTo-button']")
    private WebElement delegateToBtn;

    @FindBy(xpath="//div[@id='delegateTo-button']/select/option")
    private List<WebElement> delegateToOptions;

    @FindBy(xpath="//div[@id='scheduleBy-button']")
    private WebElement scheduleByBtn;

    @FindBy(xpath="//div[@id='scheduleBy-button']/select/option")
    private List<WebElement> scheduleByOptions;

    @FindBy(xpath="//select[@id='group']")
    private WebElement attachToGroup;

    @FindBy(xpath="//select[@id='group']/option")
    private List<WebElement>  attachToGroupDropDownOptions;

    @FindBy(xpath="//input[@id='setAlarm']")
    private WebElement setAlarmCheckBox;

    @FindBy(xpath="//a[@id='leadTime-button']")
    private WebElement alarmTimeBtn;

    @FindBy(xpath="//ul[@id='leadTime-menu']/li/a")
    private List<WebElement> alarmTimeOptions;

    @FindBy(xpath="//input[@id='timeless']")
    private WebElement timeLessCheckBox;

    @FindBy(xpath="//select[@id='priority']/preceding-sibling::a")
    private WebElement priorityBtn;

    @FindBy(xpath="//select[@id='priority']/option")
    private List<WebElement>  priorityOptions;

    @FindBy(xpath="//a[@id='btnSave']")
    private WebElement saveBtn;

    //Action methods
    public void clickOnToDoCreateOption(){
        waitForElementToBeVisible(taskTab);
       // taskTab.click();
        waitAndClickByJs(taskTab);
        waitForElementToBeVisible(toDo);
        toDo.click();
    }
    public void contactSearch(String contactName){
        sendKeys(searchBy,contactName);
    }
    public void selectFromToDoDropDown(String option){
        activityBTn.click();
        activityTypeMenu.stream().filter(e->e.getText().equalsIgnoreCase(option)).findFirst().orElseThrow(null).click();
    }

    public void selectDateTime(String date,String startTime,String endTime){
        sendKeys(dateBtn,date);
        selectMeetStartTime(startTime);
        selectMeetEndTime(endTime);
    }
    public void selectMeetStartTime(String time){
        timePickerFromBtn.click();
        timeOptions.stream().filter(e->e.getText().equalsIgnoreCase(time)).findFirst().orElseThrow(null).click();
    }
   public void selectMeetEndTime(String endTime){
       getTimePickerToBtn.click();
       timeOptions.stream().filter(e->e.getText().equalsIgnoreCase(endTime)).findFirst().orElseThrow(null).click();
   }
   public void selectReferenceSubjectForTheMeet(String subject){
       regardingDropDown.click();
       regardingOptions.stream().filter(e->e.getText().equalsIgnoreCase(subject)).findFirst().orElseThrow(null).click();
    }
    public void enterDetails(){
        detailsTextArea.sendKeys("This is a meet for a upcoming proposal discussion");
    }
    public void selectMeetDelegates(String delegateTo,String  scheduleBy){
        delegateToBtn.click();
        delegateToOptions.stream().filter(e->e.getText().equalsIgnoreCase(delegateTo)).findFirst().orElseThrow(null).click();
        scheduleByBtn.click();
        scheduleByOptions.stream().filter(e->e.getText().equalsIgnoreCase(scheduleBy)).findFirst().orElseThrow(null).click();
    }
    public void setAlarmTimeAndPriority(String time, String priority){
        setAlarmCheckBox.click();
        alarmTimeOptions.stream().filter(e->e.getText().equalsIgnoreCase(time)).findFirst().orElseThrow(null).click();
        priorityBtn.click();
        priorityOptions.stream().filter(e->e.getText().equalsIgnoreCase(priority)).findFirst().orElseThrow(null).click();
    }
    public void clickOnSaveBtn(){
        waitForElementToBeVisible(saveBtn);
        saveBtn.click();
    }


}
