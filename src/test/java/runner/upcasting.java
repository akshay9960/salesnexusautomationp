package runner;

public class upcasting {
    public static void main(String [] args){
        A a=new B();
        a.greetings();

    }
}
class A{
    void greetings(){
        System.out.println("Hello");
    }
    void welcome(){
        System.out.println("Welcome");
    }
}
class B extends A{
    void greetings(){
        System.out.println("Hi");
    }
    void wishes(){
        System.out.println("Good morning");
    }
}
