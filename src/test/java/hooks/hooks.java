package hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import utils.DriverFactory;

public class hooks {
    private WebDriver driver;
    @Before
    public void setup(){
      // driver=DriverFactory.getDriver();
    }
    @After(order=0)
    public void tearDown() throws InterruptedException {
        DriverFactory.cleanup();

    }
    @After(order=1)
    public void logOut(){
        LoginPage log=new LoginPage();
        log.logOut();
        System.out.println("LogOut Done");
    }

}
