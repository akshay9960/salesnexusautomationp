package stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.BasePage;
import pages.Contacts;
import pages.HomePage;

import java.io.IOException;

public class ContactsStep extends BasePage {
    private Contacts contacts;
    private HomePage homePage;
    public ContactsStep(Contacts contacts,HomePage homePage){
        this.contacts=contacts;
        this.homePage=homePage;
    }
    @When("user clicks on create contact option under contacts tab")
    public void user_clicks_on_create_contact_option_under_contacts_tab() {
        contacts.clickOnCreateContactOption();
    }
    @When("user is directed to Create contact window")
    public void user_is_directed_to_create_contact_window() {
        contacts.verifyUserIsOnCreateContactWindow();
    }

    @Then("verify new contact is created by searching contact name in contact search box")
    public void verify_new_contact_is_created_by_searching_contact_name_in_contact_search_box() throws InterruptedException {
        contacts.searchContactInContactSearch();
    }

    @And("user enters contact name as {string}  title as {string} ,status as {string}, lead source as {string} city as {string} and state {string} and other details")
    public void userEntersContactNameAsTitleAsStatusAsLeadSourceAsCityAsAndStateAndOtherDetails(String name, String title, String status, String leadSource, String city, String state) {
        contacts.enterCreateContactDetails(name,title,status,leadSource,city,state);
    }

    @When("user clicks on delete icon on the table")
    public void userClicksOnDeleteIconOnTheTable() throws InterruptedException {
        contacts.deleteContact();
    }

    @And("user  searches contact name in contacts search")
    public void userSearchesContactNameInContactsSearch() throws InterruptedException {
       // homePage.clickOnSkipIntroBtn();
        contacts.searchContactInContactSearch();
    }

    @Then("user should be displayed error message {string}")
    public void userShouldBeDisplayedErrorMessage(String errorMsg) {
        contacts.verifyContactIsDeleted(errorMsg);
    }

    @When("user clicks on create lookup option under contact tab")
    public void user_clicks_on_create_lookup_option_under_contact_tab() {
        contacts.clickOnCreateLookUp();
    }

    @When("user is directed to create lookup window")
    public void user_is_directed_to_create_lookup_window() {
        contacts.verifyUserIsOnCreateLookUpWindow();
    }

    @When("post and name of user is by default displayed")
    public void post_and_name_of_user_is_by_default_displayed() {
        contacts.verifyPostAndNameOfUserOnLookupWindow();
    }

    @When("user selects {string} and {string} from the dropdowns and enters address in the {string} in the textInputField")
    public void user_selects_and_from_the_dropdowns_and_enters_address_in_the_in_the_text_input_field(String address1, String startswith, String addressText) {
        contacts.setValuesInFirsRowDropDowns(address1,startswith,addressText);

    }

    @When("user clicks on searchBtn and make lookup button")
    public void user_clicks_on_search_btn_and_make_lookup_button() {
        contacts.clickOnSearchAndLookUpBtn();
    }

    @Then("user verifies that look has been created by navigating to viewlook up and validating look up table")
    public void user_verifies_that_look_has_been_created_by_navigating_to_viewlook_up_and_validating_look_up_table() {

    }

    @And("user selects  {string} and {string} from the dropdowns  and enters city as {string} in the textInputField")
    public void userSelectsAndFromTheDropdownsAndEntersCityAsInTheTextInputField(String arg0, String arg1, String arg2) {
        contacts.setValuesInSecondRowDropDowns(arg0,arg1,arg2);
    }





    @And("user {string} enters contact's information")
    public void userEntersContactSInformation(String userNo) throws IOException {
        contacts.fillConatctDetailsFromExcelFile(userNo);

    }
}
