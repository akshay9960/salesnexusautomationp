package stepDefinitions;

import io.cucumber.java.en.*;
import pages.AccountSignUp;
import pages.BasePage;
import pages.HomePage;

public class AccountSignUpStep extends BasePage {
    private AccountSignUp accountSignUp;
    private HomePage homePage;

    //Dependency injection of account signup page
    public AccountSignUpStep( AccountSignUp accountSignUp,HomePage homePage){
        this.accountSignUp=accountSignUp;
        this.homePage=homePage;
    }
    @Given("user navigates to salesnexus signup page")
    public void user_navigates_to_salesnexus_signup_page() {
        accountSignUp.navigateToAccountSignUpPage();
    }

    @When("user changes default password and clicks on continue button")
    public void user_changes_default_password_and_clicks_on_continue_button() {
        accountSignUp.changeDefaultPassword();
    }
    @When("user clicks on organize customer button")
    public void user_clicks_on_organize_customer_button() {
        accountSignUp.clickOnOrganizeCustomerBtn();
    }
    @When("user is on organize customer page and clicks on nextstep btn")
    public void user_is_on_organize_customer_page_and_clicks_on_nextstep_btn() {
        accountSignUp.nextStepBtn();
    }
    @When("user enters details for setting up company branding with city {string} state {string} Zipcode {string} and clicks on import contacts btn")
    public void user_enters_details_for_setting_up_company_branding_with_city_state_zipcode_and_clicks_on_import_contacts_btn(String city, String state, String zipcode) {
        accountSignUp.companyBrandingDetails(city,state,zipcode);
    }
    @When("user clicks on take me to free trial button")
    public void user_clicks_on_take_me_to_free_trial_button() {
        accountSignUp.clickOnTakeMeToFreeTrialBtn();
    }
    @Then("user account is created and name of user is displayed under first name column on contacts page")
    public void user_account_is_created_and_name_of_user_is_displayed_under_first_name_column_on_contacts_page() {
        accountSignUp.verifyUserFirstNameInContactsTable();
    }



    @When("user {string}  enters full name , phone number , email and clicks on free trial button")
    public void userEntersFullNamePhoneNumberEmailAndClicksOnFreeTrialButton(String row) {
        accountSignUp.signupDetails(row);
    }
}
