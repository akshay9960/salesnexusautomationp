package stepDefinitions;

import io.cucumber.java.en.*;
import pages.AccountSignUp;
import pages.BasePage;
import pages.LoginPage;

public class LoginStep extends BasePage {
    private LoginPage loginPage;

    //Dependency injection of login page
    public  LoginStep( LoginPage loginPage){
        this.loginPage=loginPage;
    }

    @Given("user navigates to sales nexus login page")
    public void user_navigates_to_sales_nexus_login_page() {
        loginPage.navigateToLoginPage();
    }
    @When("user enter valid username and password")
    public void user_enter_valid_username_and_password() {
        loginPage.login();
    }
    @When("clicks on login button")
    public void clicks_on_login_button() {
        loginPage.clickOnLoginButton();
    }
    @Then("user is directed sales nexus homepage")
    public void user_is_directed_sales_nexus_homepage() {
        loginPage.verifyUserIsOnHomePage();
    }
}
