package stepDefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;
import pages.BasePage;
import pages.Tasks;

import java.util.List;


public class TasksStep extends BasePage {
    private Tasks tasks;

    public TasksStep(Tasks task) {
        this.tasks = tasks;
    }


    @Given("user clicks on create to do option under Tasks tab")
    public void user_clicks_on_create_to_do_option_under_tasks_tab() {
        tasks.clickOnToDoCreateOption();
    }

    @When("user search contact {string} in the search bar and selects contact from the dropdown")
    public void user_search_contact_in_the_search_bar_and_selects_contact_from_the_dropdown(String name) {
        tasks.contactSearch(name);
    }

    @When("user selects {string} from the todo dropdown")
    public void user_selects_from_the_todo_dropdown(String option) {
        tasks.selectFromToDoDropDown(option);
    }

    @When("user selects meeting start date , start time,end time as")
    public void user_selects_meeting_start_date_start_time_end_time_as(DataTable dataTable) {
        List<String> meetingSchedule = dataTable.asList();
        String date = meetingSchedule.get(0);
        String startTime = meetingSchedule.get(1);
        String endTime = meetingSchedule.get(2);
        tasks.selectDateTime(date, startTime, endTime);
    }

    @When("user selects meeting subject regarding {string}")
    public void user_selects_meeting_subject_regarding(String subject) {
        tasks.selectReferenceSubjectForTheMeet(subject);
    }

    @When("user enters details of the meet")
    public void user_enters_details_of_the_meet() {
        tasks.enterDetails();
    }

    @When("user selects meet delegate to {string} and schedule by {string} and attach to group blank")
    public void user_selects_meet_delegate_to_and_schedule_by_and_attach_to_group_blank(String delegateTo, String scheduleBy) {
        tasks.selectMeetDelegates(delegateTo,scheduleBy);
    }

    @When("user set alarm {string} before the meeting and priority to {string}")
    public void user_set_alarm_before_the_meeting_and_priority_to(String time, String priority) {
        tasks.setAlarmTimeAndPriority(time,priority);
    }

    @When("user clicks on save btn")
    public void user_clicks_on_save_btn() {
        tasks.clickOnSaveBtn();
    }

    @Then("user is directed to tasks list window and created meeting should be displayed in the table")
    public void user_is_directed_to_tasks_list_window_and_created_meeting_should_be_displayed_in_the_table() {

    }

}
