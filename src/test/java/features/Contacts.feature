Feature: Verify Contacts Functionality

  Background:
    Given user navigates to sales nexus login page
    When  user enter valid username and password
    And   clicks on login button
    Then  user is directed sales nexus homepage

  @parallel
  Scenario: Create new Contact
    When  user clicks on create contact option under contacts tab
    And   user is directed to Create contact window
    And   user enters contact name as "user1"  title as "Marketing Manager" ,status as "Client", lead source as "Trade Show" city as "Atlanta" and state "AZ" and other details
    Then  verify new contact is created by searching contact name in contact search box

  @parallel
  Scenario: Delete newly created contact
    When  user clicks on create contact option under contacts tab
    And   user is directed to Create contact window
    And   user enters contact name as "user2"  title as "Marketing Manager" ,status as "Vendor", lead source as "Trade Show" city as "Atlanta" and state "AZ" and other details
    And   verify new contact is created by searching contact name in contact search box
    When user clicks on delete icon on the table
    And  user  searches contact name in contacts search
    Then user should be displayed error message "No contacts found for your search."

  @parallel
  Scenario: Create a new user defined lookup
    When  user clicks on create lookup option under contact tab
    And user is directed to create lookup window
    And post and name of user is by default displayed
    And user selects "Address 1" and "starts with" from the dropdowns and enters address in the "92 Plaza Lane" in the textInputField
    And user selects  "City" and "contains" from the dropdowns  and enters city as "Atlanta" in the textInputField
    And user clicks on searchBtn and make lookup button
    Then user verifies that look has been created by navigating to viewlook up and validating look up table






