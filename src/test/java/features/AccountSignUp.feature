Feature: Account Sign Up

  Scenario:Verify user is able to sign up on website
    Given user navigates to salesnexus signup page
    When user "1"  enters full name , phone number , email and clicks on free trial button
    And user changes default password and clicks on continue button
    And user clicks on organize customer button
    And user is on organize customer page and clicks on nextstep btn
    And user enters details for setting up company branding with city "Oxford" state "Albama" Zipcode "35013" and clicks on import contacts btn
    And user clicks on take me to free trial button
    Then user account is created and name of user is displayed under first name column on contacts page

  Scenario:Verify user is able to sign up on website
    Given user navigates to salesnexus signup page
    When user "2"  enters full name , phone number , email and clicks on free trial button
    And user changes default password and clicks on continue button
    And user clicks on organize customer button
    And user is on organize customer page and clicks on nextstep btn
    And user enters details for setting up company branding with city "Oxford" state "Albama" Zipcode "35013" and clicks on import contacts btn
    And user clicks on take me to free trial button
    Then user account is created and name of user is displayed under first name column on contacts page
