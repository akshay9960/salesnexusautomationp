Feature: Tasks

  Background:
    Given user navigates to sales nexus login page
    When  user enter valid username and password
    And   clicks on login button
    Then  user is directed sales nexus homepage


  Scenario: Verify user is able to schedule a Meeting with a contact
    Given user clicks on create to do option under Tasks tab
    When user search contact "user1detv" in the search bar and selects contact from the dropdown
    And user selects "Meeting" from the todo dropdown
    And user selects meeting start date , start time,end time as
    |"12/06/2022"|"5:30pm"|"7:00pm"|
    And user selects meeting subject regarding "Present Proposal"
    And user enters details of the meet
    And user selects meet delegate to "SN Admin" and schedule by "Sadio Mane" and attach to group blank
    And user set alarm "30 mins" before the meeting and priority to "Medium"
    And user clicks on save btn
    Then user is directed to tasks list window and created meeting should be displayed in the table






