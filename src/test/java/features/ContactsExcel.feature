Feature: Contact

  Background:
    Given user navigates to sales nexus login page
    When  user enter valid username and password
    And   clicks on login button
    Then  user is directed sales nexus homepage

  @parallel
  Scenario: Create contact
    When  user clicks on create contact option under contacts tab
    And   user is directed to Create contact window
    And   user "1" enters contact's information
    Then  verify new contact is created by searching contact name in contact search box

  @parallel
  Scenario: Create contact
    When  user clicks on create contact option under contacts tab
    And   user is directed to Create contact window
    And   user "2" enters contact's information
    Then  verify new contact is created by searching contact name in contact search box



