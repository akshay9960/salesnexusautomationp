Feature: Verify Login Functionality

  @parallel
  Scenario: Login using valid credentials
    Given user navigates to sales nexus login page
    When user enter valid username and password
    And  clicks on login button
    Then user is directed sales nexus homepage